from bluetooth import *
import subprocess
import json
import greengrasssdk

# This does nothing since it will be a long running lambda
def ble_service(event, context):
    body = {
        "message": "Go Serverless v1.0! Your function executed successfully!",
        "input": event
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    return response

def func_1():
    print("This is run from function 1")
    publish_shit_to_cloud("This is run from function 1")

def func_2():
    print("This is run from function 2")
    publish_shit_to_cloud("This is run from function 2: Check out this link https://www.youtube.com/watch?v=WqHQ_wWbG9Y")


def publish_shit_to_cloud(input):
    client.publish(
            topic='bt/cloud/connector',
            payload=input)

# Creating a greengrass core sdk client
client = greengrasssdk.client('iot-data')


# Bluetooth Server

# REQUIRED - install this on the PI
# This install the bluez daemon and the python-bluez driver to interface with from inside the lambda
# sudo apt-get install bluez python-bluez

# Define the socket
server_sock = BluetoothSocket(RFCOMM)
server_sock.bind(("", PORT_ANY))
server_sock.listen(1)

port = server_sock.getsockname()[1]

uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

# Advertise the service (This just advertising the service on the Service Discovery Protocol not the Bluetooth Discovery)
# 
advertise_service(server_sock, "SampleServer",
                  service_id=uuid,
                  service_classes=[uuid, SERIAL_PORT_CLASS],
                  profiles=[SERIAL_PORT_PROFILE],
                  #                   protocols = [ OBEX_UUID ]
                  )

# This actually makes the bluetooth on the PI advertise itself (IE. makes it display when scaned from a phone)
# HCI stand for Human Computer Interaction
subprocess.call(['sudo', 'hciconfig', 'hci0', 'piscan'])
print("Waiting for connection on RFCOMM channel %d" % port)

# We wait for a connection
client_sock, client_info = server_sock.accept()
print("Accepted connection from ", client_info)

# And lets do a forever loop on the socket and listen for data 
# This is a dumb as it gets, so if you want to imporve how you parse data this is where you would do it

# Alright fuck it lets trigger some functions from our phone (or client)
try:
    while True:
        data = client_sock.recv(1024)
        # decode the bytes to a string
        tmp = data.decode("utf-8")
        # search for hidden chars
        print (repr(tmp))
        # this shady ass language lets you do anything
        tmp = tmp.rstrip('\r\n')
        if len(data) == 0:
            break
        
        if tmp == "func_1":
            func_1()
            
        if tmp == "func_2":
            func_2()
            
        print(tmp)
        
except IOError:
    pass

print("disconnected")

# Clean your shit up when your done you filthy animal
client_sock.close()
server_sock.close()
print("all done")


